const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
    productId: {
        type: Number, // Assuming it's the ID of the product being ordered
        required: true,
    },
    email: {
        type: String,
        required: true,
    },
    count: {
        type: Number,
        required: true,
    },
    number: {
        type: Number,
        required: true,
    },
    name: {
        type: String,
        required: true,
    },
});

const Ordermodel = mongoose.model('Orders', orderSchema);

module.exports = Ordermodel;

